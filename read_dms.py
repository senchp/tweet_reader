from utils import read_aloud
from twython import TwythonStreamer
import twitter

if __name__ == "__main__":
    with open('apikey', 'r') as f:
        app_key, app_secret, oauth_token, oauth_token_secret = [s.strip() for s in f.readlines()]

    auth = twitter.OAuth(oauth_token, oauth_token_secret, app_key, app_secret)

    stream = twitter.stream.TwitterStream(auth=auth, domain='userstream.twitter.com')

    for msg in stream.user():
        if 'direct_message' in msg:
            read_aloud('{0}'.format(msg['direct_message']['text']))

    read_aloud("Uh-oh, the direct message stream stopped!")
