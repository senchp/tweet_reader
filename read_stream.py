from utils import read_aloud
from twython import TwythonStreamer

class StreamReader(TwythonStreamer):
    def on_success(self, data):
        if 'text' in data:
            read_aloud("{0}".format(data['text']))
    
    def on_error(self, status_code, data):
        print(status_code)

if __name__=="__main__":
    with open('apikey', 'r') as f:
        app_key, app_secret, oauth_token, oauth_token_secret = [s.strip() for s in f.readlines()]

    stream = StreamReader(app_key, app_secret, oauth_token, oauth_token_secret)
    stream.statuses.filter(track='SomeLivingRoom')

    read_aloud("Uh oh, the filter stream stopped!")
