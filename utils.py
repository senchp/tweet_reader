from subprocess import Popen, PIPE

def read_aloud(string):
    """Reads-out the provided string using festival
    """
    print(string)
    p1 = Popen(["flite", "-voice", "slt", "-t", "{0}".format(string)])
