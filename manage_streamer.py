from twython import Twython
from twython import TwythonRateLimitError
from subprocess import Popen, PIPE
from time import sleep
import signal
import sys
from utils import read_aloud

#start-up the streamers asynchronously:
streamer = Popen(["python", "read_stream.py"])
dm_streamer = Popen(["python", "read_dms.py"])

#prepare to kill them if a sigint or sigterm is received
def sigterm_handler(signal, frame):
    print "killing streamers"
    streamer.terminate()
    dm_streamer.terminate()
    sys.exit(0)

signal.signal(signal.SIGTERM, sigterm_handler)
signal.signal(signal.SIGINT, sigterm_handler)

with open('apikey', 'r') as f:
    app_key, app_secret, oauth_token, oauth_token_secret = [s.strip() for s in f.readlines()]

twyt = Twython(app_key, app_secret, oauth_token, oauth_token_secret)

while True:
    try:
        friends = twyt.get_friends_ids()['ids']
        followers = twyt.get_followers_ids()['ids']

        new_followers = [x for x in followers if x not in friends]
        for f in new_followers:
            twyt.create_friendship(user_id=f)
            read_aloud("Followed user %s"%f)

        if streamer.poll() is not None:
            streamer = Popen(["python", "read_stream.py"])
            read_aloud("Re-started filter stream")

        if dm_streamer.poll() is not None:
            dm_streamer = Popen(["python", "read_dms.py"])
            read_aloud("Re-started DM stream")
             

        sleep(60*5)
    except TwythonRateLimitError:
        read_aloud("Uh-oh, exceeded rate limit!")
        sleep(60*15)
